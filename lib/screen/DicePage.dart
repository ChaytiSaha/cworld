import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DiceScreen extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Dice Game',
        home: Scaffold(
          appBar:AppBar(
            title: Text('CWorld',style:
            TextStyle(fontSize: 20, color: Colors.white),),
            leading: Icon(Icons.arrow_back_ios),
            backgroundColor: Colors.blue,
          ),
          backgroundColor: Colors.blueAccent,
          body: SafeArea(
            child: DiceGame(),
          ),
        )
    );
  }
}

class DiceGame extends StatefulWidget {

  @override
  _DiceGameState createState() => _DiceGameState();
}

int LeftDiceNumber = 1;
int RightDiceNumber = 2;

class _DiceGameState extends State<DiceGame> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Dice Game',
              textAlign:TextAlign.center,
              style: (
                  TextStyle(
                    color:Colors.white,
                    fontSize: 25.0,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 2,
                    decoration: TextDecoration.none,
                  )
              ),
            ),
          ),
          SizedBox(
            height: 200,
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                    onTap: (){
                      setState(() {
                        LeftDiceNumber = Random().nextInt(6)+1;
                        RightDiceNumber = Random().nextInt(6)+1;
                      });
                    },
                    child:Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Image(image: AssetImage('assets/images/dice$LeftDiceNumber.png')),
                    )
                ),
              ),
              Expanded(
                child: GestureDetector(
                    onTap: (){
                      setState(() {
                        LeftDiceNumber = Random().nextInt(6)+1;
                        RightDiceNumber = Random().nextInt(6)+1;
                      });
                    },
                    child:Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Image(image: AssetImage('assets/images/dice$RightDiceNumber.png')),
                    )
                ),
              )
            ],
          ),
          SizedBox(
            height: 50,
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Card(
              color: Colors.lightBlueAccent,
              child: Container(
                  width: 150,
                  height: 50,
                  child: FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('End Game',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20
                      ),),
                  )
              ),
            ),
          ),
        ],
      ),
    );
  }
}


