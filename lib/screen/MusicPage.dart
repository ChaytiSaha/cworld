import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MusicScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Dice Game',
        home: Scaffold(
          appBar:AppBar(
            title: Text('CWorld',style:
            TextStyle(fontSize: 20, color: Colors.white),),
            leading: Icon(Icons.arrow_back_ios),
            backgroundColor: Colors.blue,
          ),
          backgroundColor: Colors.cyanAccent,
          body: SafeArea(
            child: PlayMusic(),
          ),
        )
    );
  }
}

class PlayMusic extends StatelessWidget {

  int soundNumber;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: Padding(
            padding: const EdgeInsets.only(left: 90.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                buildExpanded(soundNumber: 1, soundName: 'Sa' , color: Colors.indigo, w: 70),
                buildExpanded(soundNumber: 2, soundName: 'Re' , color: Colors.blue, w: 110),
                buildExpanded(soundNumber: 3, soundName: 'Ga' , color: Colors.green, w: 150),
                buildExpanded(soundNumber: 4, soundName: 'Ma' , color: Colors.teal, w: 190),
                buildExpanded(soundNumber: 5, soundName: 'Pa' , color: Colors.deepOrange, w: 230),
                buildExpanded(soundNumber: 6, soundName: 'Da' , color: Colors.amber, w: 270),
                buildExpanded(soundNumber: 7, soundName: 'Ni' , color: Colors.deepPurpleAccent, w: 310),
            ],
            ),
          ),
        ),
        SizedBox(
          height: 50,
        ),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Card(
            color: Colors.lightBlueAccent,
            child: Container(
                width: 150,
                height: 50,
                child: FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('End Playing',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20
                    ),),
                )
            ),
          ),
        ),
      ],
    );
  }

  Container buildExpanded({int soundNumber, String soundName, Color color, double w}) {
    return Container(
      width: w,
      height: 80,
      decoration: BoxDecoration(
        color: color,
        border: Border.all(
          color: Colors.lime,
          width: 3
        ),
        borderRadius: BorderRadius.only(topLeft: Radius.circular(25), bottomLeft: Radius.circular(10)),
      ),
      child: TextButton(
        onPressed: (){
          playSound(soundNumber);
        },
        child: Text(soundName,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w700,
            color: Colors.limeAccent
          ),),
      ),
    );
  }

  void playSound(int soundNumber){
    final audioCache = AudioCache();
    audioCache.play('sounds/note${soundNumber.toString()}.wav');
  }

}
