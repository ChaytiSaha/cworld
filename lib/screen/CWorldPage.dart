import 'package:cworld/screen/MusicPage.dart';
import 'package:cworld/screen/QuizScreen/QuizPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

import 'DicePage.dart';

class CworldScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('CWorld',style:
          TextStyle(fontSize: 20, color: Colors.white),),
          leading: Icon(Icons.arrow_back_ios),
          backgroundColor: Colors.blue,
        ),
        backgroundColor: Colors.blueAccent[100],
        body: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Card(
                          color: Colors.blueAccent,
                          child: Container(
                            width: 100,
                            height: 100,
                            child: FlatButton(
                              onPressed: () {
                                Navigator.push(
                                  context, CupertinoPageRoute(builder: (context) => DiceScreen(),));
                            },
                              child: Text('Play Dice',
                                textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20
                              ),),
                            )
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Card(
                          color: Colors.blueAccent,
                          child: Container(
                              width: 100,
                              height: 100,
                              child: FlatButton(
                                onPressed: () {
                                  Navigator.push(
                                    context, CupertinoPageRoute(builder: (context) => MusicScreen(),));
                                },
                                child: Text('Play Music',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20
                                  ),),
                              )
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Card(
                          color: Colors.blueAccent,
                          child: Container(
                              width: 100,
                              height: 100,
                              child: FlatButton(
                                onPressed: () {
                                  Navigator.push(
                                    context, CupertinoPageRoute(builder: (context) => QuizPage(),));
                                },
                                child: Text('Take Quiz',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20
                                  ),),
                              )
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Card(
                          color: Colors.blueAccent,
                          child: Container(
                              width: 100,
                              height: 100,
                              child: FlatButton(
                                onPressed: () {
                                  Navigator.push(
                                    context, CupertinoPageRoute(builder: (context) => DiceScreen(),));
                                },
                                child: Text('Measure BMI',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20
                                  ),),
                              )
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Card(
                          color: Colors.blueAccent,
                          child: Container(
                              width: 100,
                              height: 100,
                              child: FlatButton(
                                onPressed: () {
                                  Navigator.push(
                                    context, CupertinoPageRoute(builder: (context) => DiceScreen(),));
                                },
                                child: Text('Check Currency',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20
                                  ),),
                              )
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Card(
                          color: Colors.blueAccent,
                          child: Container(
                              width: 100,
                              height: 100,
                              child: FlatButton(
                                onPressed: () {
                                  Navigator.push(
                                    context, CupertinoPageRoute(builder: (context) => DiceScreen(),));
                                },
                                child: Text('Notebook',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20
                                  ),),
                              )
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Card(
                        color: Colors.blueAccent,
                        child: Container(
                            width: 170,
                            height: 100,
                            child: FlatButton(
                              onPressed: () {
                                showToast('Comming Soon ...',
                                  borderRadius: BorderRadius.circular(15.0),
                                  backgroundColor: Colors.blue,
                                  context: context,
                                  animation: StyledToastAnimation.scale,
                                  textStyle: TextStyle(
                                    color: Colors.yellow,
                                  )
                                );
                              },
                              child: Text('Game X',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20
                                ),),
                            )
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
