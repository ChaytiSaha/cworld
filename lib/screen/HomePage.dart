import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'CWorldPage.dart';

class HomePage extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                backgroundImage: AssetImage('assets/profile/Profile.jpg'),
                radius: 50,
              ),
              Text('Chayti Saha',
                style:(
                    TextStyle(
                        color: Colors.white,
                        fontSize: 20
                    )
                ),),
              Text('Flutter Developer',
                style:(
                    TextStyle(
                        color: Colors.white,
                        fontSize: 10,
                         letterSpacing: 2,
                    )
                ),),
              SizedBox(
                width: 150,
                child: Divider(
                  thickness: 1,
                  color: Colors.blue,
                ),
              ),
              Card(
                color: Colors.blue,
                margin: EdgeInsets.symmetric(vertical: 6.0, horizontal: 30),
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ListTile(
                    leading: Icon(Icons.phone, color:Colors.white),
                    title: Text('+88 01711122233', style: (
                        TextStyle(color: Colors.white)
                    ),),
                  ),
                ),
              ),
              Card(
                color: Colors.blue,
                margin: EdgeInsets.symmetric(vertical: 6.0, horizontal: 30),
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ListTile(
                    leading: Icon(Icons.email, color:Colors.white),
                    title: Text('exercise@gmail.com', style: (
                        TextStyle(color: Colors.white)
                    ),),
                  ),
                ),
              ),
              Card(
                color: Colors.blue,
                margin: EdgeInsets.symmetric(vertical: 6.0, horizontal: 30),
                child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child:Container(
                      width: 200,
                      height: 50,
                      color: Colors.lightBlueAccent,
                      child: FlatButton(
                        onPressed: () {
                          Navigator.push(
                            context, CupertinoPageRoute(builder: (context) => CworldScreen(),),);
                        },
                        child:Text('Welcome to CWorld',
                        style: TextStyle(
                          color: Colors.white
                        )),
                      ),
                    )
                ),
              ),
            ],
          ),
        );
  }
}



