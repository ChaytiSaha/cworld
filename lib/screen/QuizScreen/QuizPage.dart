import 'package:cworld/screen/QuizScreen/quizBank.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class QuizPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              title: Text('CWorld',style:
              TextStyle(fontSize: 20, color: Colors.white),),
              leading: Icon(Icons.arrow_back_ios),
              backgroundColor: Colors.blue,
            ),
            backgroundColor: Colors.blueAccent[100],
            body: SafeArea(
                child:QuizPageState()
            ),
        ),
    );
  }
}

class QuizPageState extends StatefulWidget {
  @override
  _QuizPageStateState createState() => _QuizPageStateState();
}

class _QuizPageStateState extends State<QuizPageState> {

  QuizBrain quizBrain = QuizBrain();
  var yes=0;
  var no=0;

  List<Icon> scoreKeeper = [];
  void checkAnswer(bool userPickedAnswer){
    setState(() {
      if(quizBrain.isFinished() == true){
        Alert(
            context: context,
            title: "শেষ",
            desc: "কুইজ সফলভাবে সম্পন্ন হয়েছে",
            content: Column(
              children:[
                Text(
                  'Correct Answer : $yes',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.green,
                      decoration: TextDecoration.none //delete underline
                  ),
                ),
                Text(
                  'Wrong Answer : $no',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.red,
                      decoration: TextDecoration.none //delete underline
                  ),
                ),
              ],
            ),
            buttons: [
              DialogButton(
                onPressed: () => Navigator.pop(context),
                child: Text(
                  'পুনরায় শুরু করুন',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 25,
                      color: Colors.white,
                      decoration: TextDecoration.none //delete underline
                  ),
                ),
              )
            ]).show();

        quizBrain.reset();
        scoreKeeper=[];
        yes=0;
        no=0;
      }

      else {
        if (quizBrain.getQuestionAnswer() == userPickedAnswer) {
          scoreKeeper.add(Icon(Icons.check, color: Colors.green));
          yes++;
        }
        else {
          scoreKeeper.add(Icon(Icons.close, color: Colors.red));
          no++;
        }
        quizBrain.nextQuestion();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Expanded(
          flex: 5,
          child: Center(
            child: Padding(
              padding: EdgeInsets.all(5.10),
              child: Text(
                quizBrain.getQuestionText(),
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 25.0,
                    color: Colors.white,
                    decoration: TextDecoration.none //delete underline
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: Container(
            color: Colors.green,
            child: Padding(
              padding: EdgeInsets.all(5.10),
              child: TextButton(
                child: Text(
                  'সত্য',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 25.0,
                      color: Colors.white
                  ),
                ),
                onPressed: (){
                  setState(() {
                    checkAnswer(true);
                  });
                },
              ),
            ),
          ),
        ),

        SizedBox(
          height: 8,
        ),

        Expanded(
          child: Container(
            color: Colors.red,
            child: Padding(
              padding: EdgeInsets.all(5.10),
              child: TextButton(
                child: Text(
                  'মিথ্যা',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 25.0,
                      color: Colors.white
                  ),
                ),
                onPressed: (){
                  setState(() {
                    checkAnswer(false);
                  });
                },
              ),
            ),
          ),
        ),

        SizedBox(
          height: 8,
        ),
        Row(
          children: scoreKeeper,
        )
      ],
    );
  }
}
