import 'package:cworld/screen/QuizScreen/questions.dart';

class QuizBrain{
  int _questionNumber = 0;
  List<Questions> _questions = [
    Questions(questionText: 'বাংলাদেশের বাণিজ্যিক রাজধানী চট্টগ্রাম', questionAnswer: true),
    Questions(questionText: 'বাংলাদেশের আয়তন ১,৪৭,৫৭০ বর্গ কি.মি.', questionAnswer: false),
    Questions(questionText: 'আয়তনে বাংলাদেশের ক্ষুদ্রতম জেলা নারায়নগঞ্জ', questionAnswer: true),
    Questions(questionText: 'বাংলাদেশের একমাত্র পাহাড়ি দ্বীপ সোনাদিয়া', questionAnswer: false),
    Questions(questionText: 'ঢাকা ১ম বাংলাদেশের রাজধানী হয় ১৭১০ সালে', questionAnswer: false),
    Questions(questionText: 'বাংলাদেশের সব চেয়ে উত্তরের স্থান বাংলাবান্ধা', questionAnswer: true),
    Questions(questionText: 'সিলেটের পূর্বনাম জালালাবাদ', questionAnswer: true),
    Questions(questionText: 'স্বদেশ প্রত্যাবর্তন দিবস ১০ ই ডিসেম্বর', questionAnswer: false),
    Questions(questionText: 'বঙ্গবন্ধুর বায়োপিকে \' বঙ্গবন্ধু \' চরিত্রে অভিনয় করছেন সিয়াম আহমেদ', questionAnswer: false),
    Questions(questionText: 'কুমিল্লার পূর্বনাম রোহিতগিরি', questionAnswer: true),
    Questions(questionText: 'সাজেক উপত্যকা বান্দরবান জেলায় অবস্থিত', questionAnswer: false),
    Questions(questionText: 'বাংলাদেশের রাজধানী ঢাকা', questionAnswer: true),
    Questions(questionText: 'বাংলাদেশে মোট নদনদী ৩০০ টি', questionAnswer: false),
    Questions(questionText: 'সোনালি আঁশ বলা হয় ধানকে', questionAnswer: false),
    Questions(questionText: 'বাংলাদেশের জাতীয় গাছ কাঁঠাল গাছ', questionAnswer: false),
    Questions(questionText: 'NULL', questionAnswer: false),
  ];

  String getQuestionText(){
      return _questions[_questionNumber].questionText;
  }

  bool getQuestionAnswer(){
    return _questions[_questionNumber].questionAnswer;
  }

  void nextQuestion(){
    if(_questionNumber<_questions.length-1){
      _questionNumber++;
    }
  }

  void reset(){
    _questionNumber = 0;
  }

  bool isFinished(){
    if(_questionNumber >=_questions.length-1){
      return true;
    }
    else{
      return false;
    }
  }
}