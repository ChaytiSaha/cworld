import 'package:cworld/screen/DrawerPage.dart';
import 'package:cworld/screen/HomePage.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
      appBar: AppBar(
        title: Text('CWorld',style:
                TextStyle(fontSize: 20, color: Colors.white),),
        backgroundColor: Colors.blue,
        ),
        backgroundColor: Colors.lightBlueAccent,
        drawer: Drawer(
          child: DrawerPage(),
        ),
        body: HomePage(),
    ),
    );
  }
}
